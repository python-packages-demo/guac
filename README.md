# [guac](https://pypi.org/project/guac)

[**guac**](https://pypi.org/project/guac)
![PyPI - Downloads](https://img.shields.io/pypi/dm/guac)Hakell-inspired monadic do-notation in Python

# Warning guac 1.0.0, the last version at time of writting (2021) is old and apparently just work with old version of pypy3 (fedora:26)
